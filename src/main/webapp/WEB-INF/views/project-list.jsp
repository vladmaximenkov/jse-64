<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../include/_header.jsp"/>

<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>

<h1>Project list</h1>

<table>
    <tr>
        <th style="width:350px">id</th>
        <th style="width:300px">name</th>
        <th>desc</th>
        <th style="width:120px">start</th>
        <th style="width:120px">finish</th>
        <th style="width:150px">status</th>
        <th style="width:75px">edit</th>
        <th style="width:75px">delete</th>
    </tr>
    <c:forEach var="project" items="${projects}">
        <tr>
            <td><c:out value="${project.id}"/></td>
            <td><c:out value="${project.name}"/></td>
            <td><c:out value="${project.description}"/></td>
            <td><fmt:formatDate value="${project.dateStart}" pattern="dd.MM.yyyy"/></td>
            <td><fmt:formatDate value="${project.dateFinish}" pattern="dd.MM.yyyy"/></td>
            <td><c:out value="${project.status.displayName}"/></td>
            <td><a href="/project/edit/${project.id}/">edit</a></td>
            <td><a href="/project/delete/${project.id}/">delete</a></td>
        </tr>
    </c:forEach>
</table>


<form action="/project/create">
    <button>Create project</button>
</form>

<jsp:include page="../include/_footer.jsp"/>