package ru.vmaksimenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.vmaksimenkov.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class TaskRepository {

    @NotNull
    private static final TaskRepository INSTANCE = new TaskRepository();

    @NotNull
    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task());
        add(new Task());
        add(new Task());
        add(new Task());
        add(new Task());
    }

    @NotNull
    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    public void create() {
        add(new Task());
    }

    public void add(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    @NotNull public Collection<Task> findAll() {
        return tasks.values();
    }

    @Nullable public Task findById(@Nullable final String id) {
        return tasks.get(id);
    }

    public void removeById(@Nullable final String id) {
        tasks.remove(id);
    }

}
