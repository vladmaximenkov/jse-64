package ru.vmaksimenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.vmaksimenkov.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class ProjectRepository {

    @NotNull private static final ProjectRepository INSTANCE = new ProjectRepository();

    @NotNull private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("Test project first"));
        add(new Project("Test second project"));
        add(new Project("Third project"));
        add(new Project("Fourth project"));
        add(new Project("Fifth project"));
    }

    @NotNull public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    public void add(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public void create() {
        add(new Project());
    }

    @NotNull public Collection<Project> findAll() {
        return projects.values();
    }

    @Nullable public Project findById(@Nullable final String id) {
        return projects.get(id);
    }

    public void removeById(@Nullable final String id) {
        projects.remove(id);
    }

}
