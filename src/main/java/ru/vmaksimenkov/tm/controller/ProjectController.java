package ru.vmaksimenkov.tm.controller;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.repository.ProjectRepository;

@Controller
public class ProjectController {

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping("/project/create")
    public String create() {
        projectRepository.add(new Project("New project " +System.currentTimeMillis()));
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        projectRepository.removeById(id);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        @Nullable final Project project = projectRepository.findById(id);
        return new ModelAndView("project-edit", "project", project);
    }

    @PostMapping("/project/edit/{id}")
    public String edit(@ModelAttribute("project") Project project, BindingResult result) {
        projectRepository.add(project);
        return "redirect:/projects";
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() { return Status.values(); }

}
